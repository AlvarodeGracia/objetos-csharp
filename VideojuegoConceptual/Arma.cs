﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideojuegoConceptual
{
    class Arma : Objeto
    {

        private int damage;


        public Arma() : base()
        {

            damage = 0;

        }

        public void setDamege(int damage)
        {
            this.damage = damage;
        }

        public int getDamage()
        {
            return damage;
        }
    }
}
