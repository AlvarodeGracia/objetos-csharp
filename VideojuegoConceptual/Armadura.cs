﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideojuegoConceptual
{
    class Armadura: Objeto
    {
        private int defensa;

        public Armadura() : base()
        {
            defensa = 0;
        }

        public int getDefensa()
        {
            return defensa;
        }

        public void setDefensa(int defensa)
        {
            this.defensa = defensa;
        }
    }
}
