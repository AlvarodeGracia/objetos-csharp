﻿using System;

namespace VideojuegoConceptual
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Videojuego Conceptual");
            Console.WriteLine("Crear Personaje");

            Console.WriteLine("Cual es su nombre:");
            string nombre = Console.ReadLine();

            Console.WriteLine("Cual es su Fuerza:");
            string fuerza = Console.ReadLine();

            Console.WriteLine("Cual es su destreza:");
            string destreza = Console.ReadLine();

            Console.WriteLine("Cual es su vida");
            string vida = Console.ReadLine();

            Console.WriteLine("Cual es su clase: \n 1-Guerrero \n 2-Mago");
            string clase = Console.ReadLine();

            Personaje per1 = new Personaje();
            per1.setNombre(nombre);
            per1.setFuerza(int.Parse(fuerza));
            per1.setDestreza(int.Parse(destreza));

            //Si hay error salta excepcion
            if (int.TryParse(vida, out int vidaint))
            {
                per1.setVida(vidaint);
            }
            per1.setClase(int.Parse(clase));

            Arma espada = new Arma();
            espada.setNombre("Espada");
            espada.setPeso(10);
            espada.setDamege(1);

            Armadura armadura = new Armadura();
            armadura.setNombre("Armadura de Cuero");
            armadura.setPeso(5);
            armadura.setDefensa(5);

            Arma arco = new Arma();
            arco.setNombre("Arco");
            arco.setPeso(4);
            arco.setDamege(1);

            Armadura camiseta = new Armadura();
            camiseta.setNombre("Camiseta");
            camiseta.setPeso(4);
            camiseta.setDefensa(1);

            if (!per1.agregarObjeto(espada)) Console.WriteLine("No se ha podido agregar a la mochila " + espada.getNombre()); ;
            if(!per1.agregarObjeto(armadura)) Console.WriteLine("No se ha podido agregar a la mochila "+armadura.getNombre());
            if (!per1.agregarObjeto(arco)) Console.WriteLine("No se ha podido agregar a la mochila " + arco.getNombre()); ;
            if (!per1.agregarObjeto(camiseta)) Console.WriteLine("No se ha podido agregar a la mochila " + camiseta.getNombre());

            Console.WriteLine("Peso de personaje: "+per1.pesoActual());
            Console.WriteLine("Peso maximo de personaje: " + per1.pesoMaximo());

            Personaje per2 = new Personaje();
            per2.setNombre("Enemigo");
            per2.setFuerza(2);
            per2.setDestreza(2);
            per2.setVida(10);
            per2.setClase(1);

            per1.atacar(per2);

            //Para que el programa no termine
            Console.WriteLine("Pulse enter para terminar...");
            Console.ReadLine();



        }



    }
}
