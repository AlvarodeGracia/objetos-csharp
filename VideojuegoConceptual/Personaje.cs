﻿using System;
using System.Collections;
using System.Text;

namespace VideojuegoConceptual
{
    class Personaje
    {

        //public string nombre {get; set;}
        public string nombre;
        private int fuerza;
        private int destreza;
        private int vida;
        private string clase;

        private ArrayList mochila;

        private Arma manoDerecha;
        private Armadura pecho;

        public Personaje()
        {
            nombre = "";
            fuerza = 0;
            destreza = 0;
            vida = 0;

            mochila = new ArrayList();

        }

        public void atacar(Personaje enemigo)
        {
            //Daño del arma por la fuerza menos la defensa de la armadura.
            Console.WriteLine(this.nombre + " esta atacando a " + enemigo.nombre);
            int damage = (manoDerecha.getDamage() * fuerza) - enemigo.getDefensa();
            enemigo.setVida(enemigo.getVida() - damage);
            Console.WriteLine("Le hace " + damage + " puntos de daño");

            if (enemigo.vida > 0)
            {
                Console.WriteLine("A " + enemigo.nombre + " le quedan " + enemigo.vida);
            }
            else
            {
                Console.WriteLine("A " + enemigo.nombre + " a muerto");
            }
        }

        public bool agregarObjeto(Objeto objeto)
        {
            bool isAdd =  false;

            int pesoAux = pesoActual() + objeto.getPeso();

            if(pesoAux < pesoMaximo())
            {
                isAdd = true;
                mochila.Add(objeto);

                if(objeto is Armadura)
                {
                    if(pecho == null)
                    {
                        pecho = (Armadura)objeto;
                        objeto.setIsEquipado(true);
                    }
                        
                }else if (objeto is Arma)
                {
                    if (manoDerecha == null)
                    {
                        manoDerecha = (Arma)objeto;
                        objeto.setIsEquipado(true);
                     
                    }
                      
                }
            }

            return isAdd;

        }

        public int pesoMaximo()
        {
            return fuerza * 10;
        }

        //public int PesoMaximo { get { return fuerza * 10; } }

        public int pesoActual()
        {
            int peso_total = 0;
            foreach(Objeto o in mochila)
            {
                if(!o.getIsEquipado())
                    peso_total += o.getPeso();
            }

            return peso_total;
        }

        public int getDefensa()
        {

            int defensa = 0;

            if(pecho != null)
            {
                defensa = pecho.getDefensa();
            }

            return defensa;
        }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string getNombre()
        {
            return nombre;
        }

        public void setFuerza(int fuerza)
        {
            this.fuerza = fuerza;
        }

        public int getFuerza()
        {
            return fuerza;
        }

        public void setDestreza(int destreza)
        {
            this.destreza = destreza;
        }

        public int getDestreza()
        {
            return destreza;
        }

        public void setVida(int vida)
        {
            this.vida = vida;
        }

        public int getVida()
        {
            return vida;
        }

        public void setMochila(ArrayList mochila)
        {
            this.mochila = mochila;
        }

        public ArrayList getMochila()
        {
            return mochila;
        }

        public Armadura getPecho()
        {
            return pecho;
        }

        public void setPEcho(Armadura armadura)
        {
            this.pecho = armadura;
        }

        public void setClase(int clase)
        {
            switch (clase)
            {
                case 1:
                    this.clase = "Guerrero";
                    break;
                case 2:
                    this.clase = "Mago";
                    break;
            }
        }

        public String getClase()
        {
            return clase;
        }
    }
}
