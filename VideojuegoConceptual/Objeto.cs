﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideojuegoConceptual
{
    abstract class Objeto
    {
         
        private String nombre;
        private int peso;
        private bool isEquipado;

        public Objeto()
        {
            nombre = "";
            peso = 0;
            isEquipado = false;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public string getNombre()
        {
            return nombre;
        }

        public int getPeso()
        {
            return peso;
        }
        
        public void setPeso(int peso)
        {
            this.peso = peso;
        }

        public bool getIsEquipado()
        {
            return isEquipado;
        }

        public void setIsEquipado(bool isEquipado)
        {
            this.isEquipado = isEquipado;
        }

    }
}
